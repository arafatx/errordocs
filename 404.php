<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sofibox - Error 404</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

        <!-- Custom Stlylesheet -->
        <link type="text/css" rel="stylesheet" href="/css/style.css" />

</head>
<body>
<div id="notfound">
                <div class="notfound">
                        <div class="notfound-404">
                                <h1>4<span>0</span>4</h1>
                        </div>
                        <h2>the page you requested could not found</h2>
                        <h3>
                                sorry, the directory <?php echo $_SERVER['REQUEST_URI']; ?> does not exist. <br>

                        </h3>
							<?php 
								//$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
                                $actual_link = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
								if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
								{
									$refuri = parse_url($_SERVER['HTTP_REFERER']);
									if($refuri['host'] == "localhost")
									{
									?>
										<h4> Please email us at <a href ='mailto:web&#109;ast%65r@s&#111;%66%69box.&#99;o%6d?subject=Reporting%20error%20from%20Sofibox%20Server&body=Reporting Error 404:%0D%0ADead%20Link%20to%20report: <?php echo $actual_link; ?>'>webmaster@sofibox.com</a> to tell us about dead link on this site. </h4>
									<?php
									}else {
									?>
										<h4>You should email someone over at <?php echo $refuri['host'] ?> and let them know they have a dead link to this site.</h4>
									<?php
									} 
								}else {
								?>
									
										<h4>If you are not sure what to do, just let us know at <a href ='mailto:web&#109;ast%65r@s&#111;%66%69box.&#99;o%6d?subject=Reporting%20error%20from%20Sofibox%20Server&body=Reporting Error 404:%0D%0ADead%20Link%20to%20report: <?php echo $actual_link; ?>'>webmaster@sofibox.com</a>.</h4>
								<?php
								}?>
																	

                        <form class="notfound-search">
                                <input type="text" placeholder="Search...">
                                <button type="button"><span></span></button>
                        </form>
                </div>
        </div>
</body>
</html>
